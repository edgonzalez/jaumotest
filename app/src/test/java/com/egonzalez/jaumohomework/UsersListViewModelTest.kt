package com.egonzalez.jaumohomework

import com.egonzalez.jaumohomework.activities.userslist.UsersListState
import com.egonzalez.jaumohomework.activities.userslist.UsersListViewModel
import io.reactivex.Observable

class UsersListViewModelTest: UsersListViewModel() {
    lateinit var usersObservable: Observable<List<Model.User>>
    public override var state = UsersListState()

    override fun newUsersObservable(): Observable<List<Model.User>> {
        return usersObservable
    }
}