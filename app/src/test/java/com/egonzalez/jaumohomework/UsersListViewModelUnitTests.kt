package com.egonzalez.jaumohomework

import com.egonzalez.jaumohomework.activities.userslist.UsersListStatus
import io.reactivex.Observable
import org.junit.Test

class UsersListViewModelUnitTests {
    val user = Model.User("n", 1, "p", "r")

    @Test
    fun users_0() {
        val viewModel = UsersListViewModelTest()
        val list = listOf<Model.User>()
        viewModel.usersObservable = Observable.just(list)

        viewModel.loadUsers()

        assert(viewModel.state.maxedList)
        assert(viewModel.state.currentUsersList.isEmpty())
        assert(viewModel.state.selected == null)
        assert(viewModel.state.status == UsersListStatus.INITIALIZED)
    }

    @Test
    fun users_1() {
        val viewModel = UsersListViewModelTest()
        val list = listOf(user)
        viewModel.usersObservable = Observable.just(list)

        viewModel.loadUsers()

        assert(viewModel.state.maxedList)
        assert(viewModel.state.currentUsersList == list)
        assert(viewModel.state.selected == null)
        assert(viewModel.state.status == UsersListStatus.INITIALIZED)
    }

    @Test
    fun users_50() {
        val viewModel = UsersListViewModelTest()
        val list = mutableListOf<Model.User>()
        for (i in 1..50) {
            list.add(user)
        }

        viewModel.usersObservable = Observable.just(list)

        viewModel.loadUsers()

        assert(!viewModel.state.maxedList)
        assert(viewModel.state.currentUsersList == list)
        assert(viewModel.state.selected == null)
        assert(viewModel.state.status == UsersListStatus.INITIALIZED)
    }

    @Test
    fun users_more_0() {
        val viewModel = UsersListViewModelTest()
        val list = mutableListOf<Model.User>()
        for (i in 1..50) {
            list.add(user)
        }

        viewModel.usersObservable = Observable.just(listOf())
        viewModel.state.currentUsersList = list

        viewModel.loadMoreUsers()

        assert(viewModel.state.maxedList)
        assert(viewModel.state.currentUsersList == list)
        assert(viewModel.state.selected == null)
        assert(viewModel.state.status == UsersListStatus.ADDED_PAGE)
    }

    @Test
    fun users_more_50() {
        val viewModel = UsersListViewModelTest()
        val list = mutableListOf<Model.User>()
        for (i in 1..50) {
            list.add(user)
        }

        viewModel.usersObservable = Observable.just(list)
        viewModel.state.currentUsersList = list

        viewModel.loadMoreUsers()

        assert(!viewModel.state.maxedList)
        assert(viewModel.state.currentUsersList.size == 100)
        assert(viewModel.state.selected == null)
        assert(viewModel.state.status == UsersListStatus.ADDED_PAGE)
    }

    @Test
    fun users_click() {
        val viewModel = UsersListViewModelTest()

        viewModel.onUserClicked(10)

        assert(viewModel.state.selected == 10)
        assert(viewModel.state.status == UsersListStatus.SELECTED)
    }
}
