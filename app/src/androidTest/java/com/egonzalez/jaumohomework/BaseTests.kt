package com.egonzalez.jaumohomework

import android.app.Activity
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import com.egonzalez.jaumohomework.activities.userslist.UsersListActivity
import com.egonzalez.jaumohomework.activities.userslist.UsersListAdapter
import com.egonzalez.jaumohomework.base.HttpClientFactory
import com.jakewharton.espresso.OkHttp3IdlingResource
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress


open class BaseTests {
    private var mockWebServer: MockWebServer? = null
    private var idlingResource: OkHttp3IdlingResource? = null
    private lateinit var activity: Activity

    fun startActivity() {
        val intent = Intent(InstrumentationRegistry.getTargetContext(), UsersListActivity::class.java)
        activity = ActivityTestRule(UsersListActivity::class.java).launchActivity(intent)
    }

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer?.start(InetAddress.getByName("localhost"), 55555)

        idlingResource = OkHttp3IdlingResource.create("OkHttp", HttpClientFactory.build())
        IdlingRegistry.getInstance().register(idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResource)
        idlingResource = null

        mockWebServer?.shutdown()
        mockWebServer = null
    }

    fun addResponse(statusCode: Int, responseFile: String) {
        mockWebServer?.enqueue(MockResponse()
                .setResponseCode(statusCode)
                .addHeader("Content-Type", "application/json")
                .setBody(getResponseAsString(responseFile)))
    }

    private fun getResponseAsString(file: String): String? {
        try {
            val stream = InstrumentationRegistry.getContext().assets.open(file)
            if (stream != null) {
                val sb = StringBuilder()
                var line: String?

                val br = BufferedReader(InputStreamReader(stream))
                line = br.readLine()

                while (line != null) {
                    sb.append(line)
                    line = br.readLine()
                }
                br.close()

                return sb.toString()
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }

        return null
    }

    fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {
        return RecyclerViewMatcher(recyclerViewId)
    }

    protected fun getText(id: Int): String?{
        return InstrumentationRegistry.getTargetContext().getString(id)
    }

    protected fun getQuantityText(id: Int, quantity: Int): String?{
        return InstrumentationRegistry.getTargetContext()
                .resources.getQuantityString(id, quantity, quantity)
    }

    protected fun checkDisplayedText(text: String?){
        onView(withText(text)).check(matches(isDisplayed()))
    }

    protected fun checkListCount(count: Int){
        val recyclerView: RecyclerView = activity.findViewById(R.id.users_list)
        assertTrue(count == recyclerView.adapter.itemCount)
    }

    protected fun checkListItemText(position: Int, text: String?){
        onView(withRecyclerView(R.id.users_list).atPosition(position))
                .check(matches(hasDescendant(withText(text))))
    }

    protected fun goToListItem(position: Int) {
        onView(withId(R.id.users_list))
                .perform(RecyclerViewActions.scrollToPosition<UsersListAdapter.UsersListViewHolder>(position))
    }

    protected fun clickListItem(position: Int){
        onView(withRecyclerView(R.id.users_list).atPosition(position))
                .perform(click())
    }

    protected fun checkListUser(position: Int, name: String, age: Int) {
        checkListItemText(position, name)
        checkListItemText(position, getQuantityText(R.plurals.amount_years, age))
    }

    protected fun checkListUserDetail(name: String, age: Int, region: String) {
        checkDisplayedText(name)
        checkDisplayedText(getQuantityText(R.plurals.amount_years, age))
        checkDisplayedText(region)
    }
}