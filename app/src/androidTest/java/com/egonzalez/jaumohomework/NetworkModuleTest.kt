package com.egonzalez.jaumohomework

import com.egonzalez.jaumohomework.base.HttpClientFactory
import com.egonzalez.jaumohomework.base.NetworkModule
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


/**
 * Module which provides all required dependencies about network
 */
@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused")
class NetworkModuleTest : NetworkModule() {
    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Singleton
    override fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://localhost:55555")
                .client(HttpClientFactory.build())
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }
}