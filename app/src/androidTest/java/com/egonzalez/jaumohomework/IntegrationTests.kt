package com.egonzalez.jaumohomework

import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class IntegrationTests: BaseTests() {
    @Test
    fun ErrorTest() {
        addResponse(500, "users_0.json")

        startActivity()

        checkDisplayedText(getText(R.string.try_again_later))
    }

    @Test
    fun NetworkErrorTest() {
        startActivity()

        checkDisplayedText(getText(R.string.network_error_pull))
    }

    @Test
    fun Users0Test() {
        addResponse(200, "users_0.json")

        startActivity()

        checkDisplayedText(getText(R.string.no_users))
    }

    @Test
    fun Users1Test() {
        addResponse(200, "users_1.json")

        startActivity()

        checkListCount(1)
        checkListUser(0, "Levoslav", 22)

        clickListItem(0)

        checkListUserDetail("Levoslav", 22, "Slovakia")
    }

    @Test
    fun Users50Test() {
        addResponse(200, "users_50.json")
        addResponse(200, "users_0.json")

        startActivity()

        checkListCount(51)
        goToListItem(50)

        checkListCount(50)
    }

    @Test
    fun Users500Test() {
        for(i in 1..10) {
            addResponse(200, "users_50.json")
        }

        startActivity()

        for(i in 1..9) {
            checkListCount(i * 50 + 1)
            goToListItem(i * 50)
        }

        checkListCount(500)
    }
}