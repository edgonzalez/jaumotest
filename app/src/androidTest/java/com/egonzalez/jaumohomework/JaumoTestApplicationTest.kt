package com.egonzalez.jaumohomework

import com.egonzalez.jaumohomework.base.DaggerViewModelInjector
import com.egonzalez.jaumohomework.base.HttpClientFactory
import com.egonzalez.jaumohomework.base.NetworkModule
import com.egonzalez.jaumohomework.base.ViewModelInjector
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

class JaumoTestApplicationTest: JaumoTestApplication() {

    override fun createInjector(): ViewModelInjector {
        return DaggerViewModelInjector
                .builder()
                .networkModule(NetworkModuleTest())
                .build()
    }
}