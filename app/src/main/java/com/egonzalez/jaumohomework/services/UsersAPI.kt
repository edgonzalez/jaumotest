package com.egonzalez.jaumohomework.services

import com.egonzalez.jaumohomework.Model
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersAPI {
    @GET("api/?ext")
    fun getUsers(@Query("amount") action: Number): Observable<List<Model.User>>
}