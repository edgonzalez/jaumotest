package com.egonzalez.jaumohomework.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.egonzalez.jaumohomework.Model
import com.egonzalez.jaumohomework.R
import com.squareup.picasso.Picasso

class UserListItemView  @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0) : ConstraintLayout(context, attrs, defStyle)  {

    init {
        LayoutInflater.from(context)
                .inflate(R.layout.users_list_item_view, this, true)
    }

    fun setUser(user: Model.User) {
        val image: ImageView = findViewById(R.id.users_list_item_image)
        val nameTextView: TextView = findViewById(R.id.users_list_item_name)
        val ageTextView: TextView = findViewById(R.id.users_list_item_age)

        nameTextView.text = user.name
        ageTextView.text = resources.getQuantityString(R.plurals.amount_years, user.age, user.age)

        Picasso.with(context).load(user.photo).into(image)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }
}