package com.egonzalez.jaumohomework

import android.app.Application
import com.egonzalez.jaumohomework.base.DaggerViewModelInjector
import com.egonzalez.jaumohomework.base.HttpClientFactory
import com.egonzalez.jaumohomework.base.NetworkModule
import com.egonzalez.jaumohomework.base.ViewModelInjector
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

open class JaumoTestApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        injector = createInjector()

        HttpClientFactory(applicationContext)

        Picasso.setSingletonInstance(Picasso.Builder(applicationContext)
                .downloader(OkHttp3Downloader(HttpClientFactory.build()))
                .loggingEnabled(true)
                .build())
    }

    protected open fun createInjector(): ViewModelInjector{
        return DaggerViewModelInjector
                .builder()
                .networkModule(NetworkModule())
                .build()
    }

    companion object {
        private var injector: ViewModelInjector? = null

        @JvmStatic fun getInjector(): ViewModelInjector? {
            return injector
        }
    }
}