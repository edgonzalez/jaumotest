package com.egonzalez.jaumohomework.base

import android.content.Context
import android.os.Build
import android.util.Log
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import java.security.KeyStore
import java.util.*
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class HttpClientFactory(context: Context) {

    companion object {
        @JvmStatic
        private lateinit var okHttpClient: OkHttpClient

        fun build(): OkHttpClient {
            return okHttpClient
        }
    }

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(logging)

        if (isTLSEnableNeeded()) {
            enableTls12(httpClient)
            installProvider(context)
        }

        okHttpClient = httpClient.build()
    }

    /**
     * True if enabling TLS is needed on current device (SDK version >= 16 and < 22)
     */
    private fun isTLSEnableNeeded(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP
    }

    /**
     * Enable TLS on the OKHttp builder by setting a custom SocketFactory
     */
    private fun enableTls12(client: OkHttpClient.Builder): OkHttpClient.Builder {
        try {
            val trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)
            val trustManagers = trustManagerFactory.trustManagers
            if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
                throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
            }
            val trustManager = trustManagers[0] as X509TrustManager

            client.sslSocketFactory(TLSSocketFactory(), trustManager)

            val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1)
                    .build()

            val specs = ArrayList<ConnectionSpec>()
            specs.add(cs)
            specs.add(ConnectionSpec.COMPATIBLE_TLS)
            specs.add(ConnectionSpec.CLEARTEXT)

            client.connectionSpecs(specs)
        } catch (exc: Exception) {
            Log.e("HttpClientFactory", "Error while setting TLS")
        }

        return client
    }

    /**
     * This method try to install security patch to support LTS 1.1 and TLS 1.2 on api level >=16 and api level < 21
     * @param context The context Application
     */
    private fun installProvider(context: Context) {
        try {
            ProviderInstaller.installIfNeeded(context)
        } catch (e: GooglePlayServicesRepairableException) {

            // Indicates that Google Play services is out of date, disabled, etc.
            Log.e("HttpClientFactory", "GooglePlayServicesRepairableException: Google Play services is out of date or disabled")

        } catch (e: GooglePlayServicesNotAvailableException) {
            // Indicates a non-recoverable error; the ProviderInstaller is not able
            // to install an up-to-date Provider.

            Log.e("HttpClientFactory", "GooglePlayServicesNotAvailableException: The ProviderInstaller is not able to install")
        }
    }
}