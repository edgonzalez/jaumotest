package com.egonzalez.jaumohomework.base

import android.arch.lifecycle.ViewModel
import com.egonzalez.jaumohomework.JaumoTestApplication
import com.egonzalez.jaumohomework.activities.userslist.UsersListViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector? = JaumoTestApplication.getInjector()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is UsersListViewModel -> injector?.inject(this)
        }
    }
}