package com.egonzalez.jaumohomework

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

object Model {
    @Parcelize
    data class User(val name: String, val age: Int, val photo: String, val region: String) : Parcelable
}