package com.egonzalez.jaumohomework.activities.userslist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.HORIZONTAL
import android.support.v7.widget.RecyclerView.VERTICAL
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.egonzalez.jaumohomework.Model
import com.egonzalez.jaumohomework.R
import com.egonzalez.jaumohomework.activities.userdetail.UserDetailActivity

class UsersListActivity : AppCompatActivity() {
    private lateinit var errorMessage: TextView
    private lateinit var usersListRecyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var viewModel: UsersListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_users_list)

        setupViews()

        viewModel = ViewModelProviders.of(this).get(UsersListViewModel::class.java)
        viewModel.usersListModel = UsersListModelImpl()
        (viewModel.usersListModel as UsersListModelImpl).getUsersListLiveData().observe(this, Observer {
            it?.let { onStateChanged(it) }
        })
    }

    private fun setupViews() {
        errorMessage = findViewById(R.id.users_list_error_message)

        swipeRefresh = findViewById(R.id.swiperefresh)
        swipeRefresh.setOnRefreshListener {
            val success = viewModel.loadUsers()
            if (!success) {
                swipeRefresh.isRefreshing = false
            }
        }

        val usersListAdapter = UsersListAdapter(object: UsersListAdapter.ActionListener {
            override fun onFetchListener() {
                viewModel.loadMoreUsers()
            }

            override fun onClickListener(index: Int) {
                viewModel.onUserClicked(index)
            }
        })

        val usersListLayoutManager = GridLayoutManager(baseContext, 2)
        usersListLayoutManager.spanSizeLookup = usersListAdapter.getSpanSizeLookup()

        usersListRecyclerView = findViewById<RecyclerView>(R.id.users_list).apply {
            layoutManager = usersListLayoutManager
            adapter = usersListAdapter
        }

        val offsetDrawable = ContextCompat.getDrawable(applicationContext, R.drawable.square2dp)

        val verticalOffset = DividerItemDecoration(baseContext, VERTICAL)
        offsetDrawable?.let { verticalOffset.setDrawable(offsetDrawable) }

        val horizontalOffset = DividerItemDecoration(baseContext, HORIZONTAL)
        offsetDrawable?.let { horizontalOffset.setDrawable(offsetDrawable) }

        usersListRecyclerView.addItemDecoration(verticalOffset)
        usersListRecyclerView.addItemDecoration(horizontalOffset)
    }

    private fun onStateChanged(state: UsersListState) {
        when(state.status) {
            UsersListStatus.NETWORK_ERROR -> {showError(getString(R.string.network_error_pull))}
            UsersListStatus.ERROR -> {showError(getString(R.string.try_again_later))}
            UsersListStatus.INITIALIZED -> {updateUserList(state.currentUsersList, !state.maxedList, false)}
            UsersListStatus.ADDED_PAGE -> {updateUserList(state.currentUsersList, !state.maxedList, true)}
            UsersListStatus.SELECTED -> {userSelected(state.currentUsersList, state.selected)}
        }
    }

    private fun showError(message: String) {
        errorMessage.text = message
        swipeRefresh.isRefreshing = false
        usersListRecyclerView.visibility = GONE
        errorMessage.visibility = VISIBLE
    }

    private fun showUsersList() {
        swipeRefresh.isRefreshing = false
        errorMessage.visibility = GONE
        usersListRecyclerView.visibility = VISIBLE
    }

    private fun updateUserList(usersList: List<Model.User>, allowFetch: Boolean, addedData: Boolean) {
        if (usersList.isEmpty()) {
            showError(getString(R.string.no_users))

        } else {
            showUsersList()

            val adapter: UsersListAdapter = usersListRecyclerView.adapter as UsersListAdapter
            adapter.paginationEnabled = allowFetch

            if (addedData) {
                adapter.addedData(usersList)

            } else {
                adapter.setData(usersList)
            }

        }
    }

    private fun userSelected(usersList: List<Model.User>, index: Int?) {
        index?.let { goToUserDetailActivity(usersList[index]) }
    }

    private fun goToUserDetailActivity(user: Model.User) {
        val newIntent = UserDetailActivity.newIntent(applicationContext, user)
        startActivity(newIntent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.hold)
    }
}