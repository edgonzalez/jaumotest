package com.egonzalez.jaumohomework.activities.userslist

import com.egonzalez.jaumohomework.Model

data class UsersListState (
    var currentUsersList: List<Model.User> = listOf(),
    var maxedList: Boolean = false,
    var selected: Int? = null,
    var status: UsersListStatus = UsersListStatus.INITIALIZED
)