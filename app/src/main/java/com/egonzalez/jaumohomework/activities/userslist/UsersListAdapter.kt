package com.egonzalez.jaumohomework.activities.userslist

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.egonzalez.jaumohomework.Model
import com.egonzalez.jaumohomework.views.UserListItemView
import com.egonzalez.jaumohomework.R

class UsersListAdapter(private val actionListener: ActionListener) : RecyclerView.Adapter<UsersListAdapter.UsersListViewHolder>() {
    class UsersListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface ActionListener {
        fun onClickListener(index: Int)
        fun onFetchListener()
    }

    private val VIEW_TYPE_USER = 0
    private val VIEW_TYPE_PROGRESS = 1

    private var usersList = listOf<Model.User>()
    private var fetching = false
    var paginationEnabled = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersListViewHolder {
        val view = if (viewType == VIEW_TYPE_USER) {
            UserListItemView(parent.context)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.progress_list_item_view, parent, false)
        }

        return UsersListViewHolder(view)
    }

    override fun getItemCount(): Int {
        var count = usersList.size
        if (paginationEnabled) {
            count += 1
        }

        return count
    }

    override fun getItemViewType(position: Int): Int {
        if (paginationEnabled && position == usersList.size) {
            return VIEW_TYPE_PROGRESS
        }

        return VIEW_TYPE_USER
    }

    override fun onBindViewHolder(holder: UsersListViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_USER) {
            val currUser = usersList[position]
            (holder.itemView as UserListItemView).setUser(currUser)
            holder.itemView.setOnClickListener { actionListener.onClickListener(position) }

        } else {
            if (!fetching) {
                actionListener.onFetchListener()
                fetching = true
            }
        }
    }

    fun getSpanSizeLookup(): GridLayoutManager.SpanSizeLookup {
        return object: GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(pos: Int): Int {
                if (getItemViewType(pos) == VIEW_TYPE_PROGRESS)
                    return 2

                return 1
            }
        }
    }

    fun setData(usersList: List<Model.User>) {
        fetching = false

        this.usersList = usersList
        notifyDataSetChanged()
    }

    fun addedData(usersList: List<Model.User>) {
        if (this.usersList.isEmpty()) {
            setData(usersList)

        } else {
            fetching = false

            val positionStart = this.usersList.size
            val itemCount = usersList.size - this.usersList.size

            this.usersList = usersList

            notifyItemChanged(positionStart)
            notifyItemRangeInserted(positionStart, itemCount)
        }
    }
}