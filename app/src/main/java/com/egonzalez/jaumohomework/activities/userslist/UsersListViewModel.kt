package com.egonzalez.jaumohomework.activities.userslist

import com.egonzalez.jaumohomework.Model
import com.egonzalez.jaumohomework.base.BaseViewModel
import com.egonzalez.jaumohomework.services.UsersAPI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject


open class UsersListViewModel: BaseViewModel() {
    @Inject
    lateinit var usersAPI: UsersAPI

    private val PAGE_SIZE = 50
    private val MAX_PAGES = 10

    var usersListModel: UsersListModel? = null

    private var subscription: Disposable? = null

    protected open var state: UsersListState = UsersListState()

    protected open fun newUsersObservable(): Observable<List<Model.User>> {
        return usersAPI.getUsers(PAGE_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadUsers(): Boolean {
        if (subscription == null) {
            subscription = newUsersObservable().subscribe(
                { users -> onGetUsersSuccess(users) },
                { e -> onGetUsersError(e) }
            )
            return true
        }

        return false
    }

    fun loadMoreUsers(): Boolean {
        if (subscription == null) {
            subscription = newUsersObservable().subscribe(
                    { users -> onGetUsersSuccess(users, true) },
                    { e -> onGetUsersError(e) }
            )
            return true
        }

        return false
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
        subscription = null
    }

    private fun onGetUsersSuccess(usersList: List<Model.User>, addPage: Boolean = false){
        subscription?.dispose()
        subscription = null

        if (addPage) {
            state.currentUsersList += usersList
            state.status = UsersListStatus.ADDED_PAGE
        } else {
            state.currentUsersList = usersList
            state.status = UsersListStatus.INITIALIZED
        }

        state.maxedList = usersList.size < PAGE_SIZE || state.currentUsersList.size >= MAX_PAGES*PAGE_SIZE

        usersListModel?.postValue(state)
    }

    private fun onGetUsersError(e: Throwable) {
        subscription?.dispose()
        subscription = null

        if (e is IOException || e is TimeoutException) {
            state.status = UsersListStatus.NETWORK_ERROR
        } else {
            state.status = UsersListStatus.ERROR
        }

        usersListModel?.postValue(state)
    }

    fun onUserClicked(index: Int) {
        subscription?.dispose()
        subscription = null

        state.status = UsersListStatus.SELECTED
        state.selected = index

        usersListModel?.postValue(state)
    }
}