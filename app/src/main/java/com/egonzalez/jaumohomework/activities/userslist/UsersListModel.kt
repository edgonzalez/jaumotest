package com.egonzalez.jaumohomework.activities.userslist

import android.arch.lifecycle.MutableLiveData
import com.egonzalez.jaumohomework.Model
import com.egonzalez.jaumohomework.services.UsersAPI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject


interface UsersListModel {
    fun getUsersListLiveData(): MutableLiveData<UsersListState>
    fun postValue(state: UsersListState)
}