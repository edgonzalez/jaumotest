package com.egonzalez.jaumohomework.activities.userslist

enum class UsersListStatus {
    NETWORK_ERROR,
    ERROR,
    INITIALIZED,
    ADDED_PAGE,
    SELECTED
}

