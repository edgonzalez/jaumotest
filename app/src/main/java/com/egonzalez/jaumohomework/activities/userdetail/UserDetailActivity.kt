package com.egonzalez.jaumohomework.activities.userdetail

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.egonzalez.jaumohomework.R
import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.egonzalez.jaumohomework.Model
import com.squareup.picasso.Picasso


class UserDetailActivity : AppCompatActivity() {

    private lateinit var user: Model.User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_user_detail)

        user = intent.getParcelableExtra(INTENT_USER)

        setupViews()
    }

    override fun onPause() {
        super.onPause()

        if (isFinishing) {
            overridePendingTransition(R.anim.hold, R.anim.slide_out_down)
        }
    }

    private fun setupViews() {
        val image: ImageView = findViewById(R.id.user_detail_image)
        val nameTextView: TextView = findViewById(R.id.user_detail_name)
        val ageTextView: TextView = findViewById(R.id.user_detail_age)
        val regionTextView: TextView = findViewById(R.id.user_detail_region)

        Picasso.with(baseContext).load(user.photo).into(image)
        nameTextView.text = user.name
        ageTextView.text = resources.getQuantityString(R.plurals.amount_years, user.age, user.age)
        regionTextView.text = user.region
    }

    companion object {
        private const val INTENT_USER = "user"

        @JvmStatic
        fun newIntent(context: Context, user: Model.User): Intent {
            val intent = Intent(context, UserDetailActivity::class.java)
            intent.putExtra(INTENT_USER, user)
            return intent
        }
    }
}