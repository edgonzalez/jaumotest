### Jaumo Test ###

This is the Jaumo Test resolution (by Emiliano Gonz�lez).

### How to build and run ###

1 - Download and install [Android Studio](https://developer.android.com/studio/).

2 - Once installed, launch and select "Open an existing Android Studio project".

3 - Press "Run app" at top menu.